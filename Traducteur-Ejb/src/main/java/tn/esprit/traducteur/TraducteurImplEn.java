package tn.esprit.traducteur;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless(name="en")
public class TraducteurImplEn implements 
				TraducteurModule{

	@EJB
	Dictionnaire dictionnaire;
	
	@Override
	public String traduire(String mot) {
		// TODO Auto-generated method stub
		return dictionnaire.traduireEnFr(mot);
	}

}
