package tn.esprit.traducteur;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class Dictionnaire {

	
	private Map<String, String> values = new HashMap<>();
	
	@PostConstruct
	public void init(){
		values.put("Salut", "Hello");
		values.put("Bonjour", "Good Morning");
		values.put("Bonsoir", "Good Evening");
	}
	
	public String traduireFrEn(String mot){
		return values.get(mot);
	}
	
	public String traduireEnFr(String mot){

		Set<String> keys = values.keySet();
		for (String k : keys) {
			if (values.get(k).equals(mot)) {
				return k;
			}
		}
		
		return null;
	}
	
	public boolean isFrench(String mot){
		return values.containsKey(mot);
	}
	
	public boolean isEnglish(String mot){
		return values.containsValue(mot);
	}
	
}
