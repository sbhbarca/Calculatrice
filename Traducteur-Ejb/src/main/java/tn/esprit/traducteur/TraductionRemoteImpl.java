package tn.esprit.traducteur;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class TraductionRemoteImpl implements TraducteurRemote{

	
	@EJB(beanName="fr")
	TraducteurModule traducteurfr;
	
	@EJB(beanName="en")
	TraducteurModule traducteuren;
	
	@EJB
	Dictionnaire dictionnaire;
	
	TraducteurModule traducteurModule;
	
	@PostConstruct
	public void init(){
		traducteurModule = traducteurfr;
	}
	
	@Override
	public String traduire(String mot) {
		// TODO Auto-generated method stub
		if (dictionnaire.isFrench(mot)) {
			traducteurModule= traducteurfr;
		}
		if (dictionnaire.isEnglish(mot)) {
			traducteurModule=traducteuren;
		}
		return traducteurModule.traduire(mot);
	}

}
