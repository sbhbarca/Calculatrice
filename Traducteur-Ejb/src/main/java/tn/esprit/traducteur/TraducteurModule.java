package tn.esprit.traducteur;

import javax.ejb.Local;
import javax.ejb.Remote;

@Local
public interface TraducteurModule {

	public String traduire(String mot);
}
