package tn.esprit.test;

import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.traducteur.TraducteurModule;
import tn.esprit.traducteur.TraducteurRemote;

public class TraducteurTest {

	static Logger logger = Logger.getLogger(TraducteurTest.class.getName());
	
	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		InitialContext context = new InitialContext();
		
		TraducteurRemote moduleRemote = (TraducteurRemote) context.lookup("Traducteur-Ejb/TraductionRemoteImpl!tn.esprit.traducteur.TraducteurRemote");
		
		logger.info(moduleRemote.traduire("Hello"));
		logger.info(moduleRemote.traduire("Bonjour"));
		logger.info(moduleRemote.traduire("fff"));
	}

}
