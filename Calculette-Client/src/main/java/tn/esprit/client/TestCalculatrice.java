package tn.esprit.client;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.ejb.CalculatriceRemote;

public class TestCalculatrice {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub

		InitialContext context = new InitialContext();
		CalculatriceRemote calculatriceRemote = (CalculatriceRemote) 
				context.lookup("Calculatrice/CalculatriceImpl!tn.esprit.ejb.CalculatriceRemote");
		
		System.out.println(calculatriceRemote.add(5, 3));
	}

}
