package tn.esprit.ejb;

import javax.ejb.Local;
import javax.ejb.Remote;

@Local
public interface CalculatriceLocal {

	public int add(int a, int b);
}
