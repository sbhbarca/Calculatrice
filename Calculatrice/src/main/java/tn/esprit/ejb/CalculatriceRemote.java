package tn.esprit.ejb;

import javax.ejb.Remote;

@Remote
public interface CalculatriceRemote {

	public int add(int a, int b);
}
